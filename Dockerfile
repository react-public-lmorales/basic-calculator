# Usa Node como base para compilar la aplicación
FROM node as builder

WORKDIR /app

# Copia los archivos del proyecto al contenedor
COPY . .

# Instala las dependencias y compila la aplicación
RUN npm install && npm run build
